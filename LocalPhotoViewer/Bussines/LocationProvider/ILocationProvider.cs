﻿using System.Threading.Tasks;
using LocalPhotoViewer.DTO;

namespace LocalPhotoViewer.BussinesLogick.LocationProvider
{
    public interface ILocationProvider
    {
        Task<LocationDTO> GetCurrentLocationAsync();

    }
}
