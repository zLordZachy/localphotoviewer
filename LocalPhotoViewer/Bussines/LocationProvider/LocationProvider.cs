﻿using System;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using LocalPhotoViewer.BussinesLogick.LocationProvider;
using LocalPhotoViewer.DTO;

namespace LocalPhotoViewer.Bussines.LocationProvider
{
    public class LocationProvider : ILocationProvider
    {
        public async Task<LocationDTO> GetCurrentLocationAsync()
        {
            try
            {
                Geolocator geolocator = new Geolocator();
                Geoposition location = await geolocator.GetGeopositionAsync();

                if (location == null)
                    return null;

                LocationDTO locationDTO = new LocationDTO
                {
                    Longitude = location.Coordinate.Point.Position.Longitude,
                    Altitude = location.Coordinate.Point.Position.Altitude,
                    Latitude = location.Coordinate.Point.Position.Latitude,
                };

                if (location.CivicAddress != null)
                {
                    locationDTO.State = location.CivicAddress.State;
                    locationDTO.City = location.CivicAddress.State;
                    locationDTO.Country = location.CivicAddress.Country;
                }

                return locationDTO;
            }
            catch (Exception)
            {
                return null;
            }
       
        }
    }
}
