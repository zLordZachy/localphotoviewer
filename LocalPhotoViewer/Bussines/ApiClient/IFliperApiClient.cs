﻿using System.Threading.Tasks;
using LocalPhotoViewer.DTO;

namespace LocalPhotoViewer.Bussines.ApiClient
{
    public interface IFliperApiClient
    {
        Task<RootObject> GetImagesAsync(double longitude, double latitude, double radius, int page, bool sortAsc);
    }
}
