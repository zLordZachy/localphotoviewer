﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using LocalPhotoViewer.DTO;
using Newtonsoft.Json;

namespace LocalPhotoViewer.Bussines.ApiClient
{
    public class FliperApiClient : IFliperApiClient
    {
      
        public async Task<RootObject> GetImagesAsync(double longitude, double latitude, double radius, int page,
            bool sortAsc)
        {
            // My api key cb449e6012a8386e2fbdeddd3a697cda
            // My secret key 183a7c3c4a37a7e5
            try
            {
                string http = "https";
                int perPage = 300;
                string sort = sortAsc ? "asc" : "desc";
                string apiKey = "cb449e6012a8386e2fbdeddd3a697cda";
                string provideUri =
                    $"{http}://api.flickr.com/services/rest/?method=flickr.photos.search&api_key={apiKey}&sort=date-posted-{sort}&privacy_filter=1&lat={latitude}&lon={longitude}&radius={radius}&per_page={perPage}&page={page}&format=json&nojsoncallback=1&extras=geo";
                HttpClient client = new HttpClient();
                string jsonstring = await client.GetStringAsync(provideUri);
                return JsonConvert.DeserializeObject<RootObject>(jsonstring);
            }
            catch (Exception)
            {
               return new RootObject{stat = "fail"};
            }
        }

        //public async Task<PhotoLocationRoot> GetPhotoLocationAsync(string photoId)
        //{
        //    try
        //    {
        //        string http = "https";
        //        string apiKey = "cb449e6012a8386e2fbdeddd3a697cda";
        //        string provideUri =
        //            $"{http}://api.flickr.com/services/rest/?method=flickr.photos.geo.getLocation&api_key={apiKey}&photo_id={photoId}&format=json&nojsoncallback=1";

        //        HttpClient client = new HttpClient();
        //        string jsonstring = await client.GetStringAsync(provideUri);
        //        return JsonConvert.DeserializeObject<PhotoLocationRoot>(jsonstring);
        //    }
        //    catch (Exception)
        //    {
        //        return new PhotoLocationRoot {stat = "fail"};
        //    }
        //}
    }
}
