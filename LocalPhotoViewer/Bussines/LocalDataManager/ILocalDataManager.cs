﻿using System.Threading.Tasks;
using LocalPhotoViewer.DTO;

namespace LocalPhotoViewer.Bussines.LocalDataManager
{
    public interface ILocalDataManager
    {
        Task SaveLocalData(LocalDataDTO localDataDTO);
        Task<LocalDataDTO> LoadLocalData();
    }
}
