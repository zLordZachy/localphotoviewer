﻿using System;
using System.Threading.Tasks;
using Windows.Storage;
using LocalPhotoViewer.DTO;
using Newtonsoft.Json;

namespace LocalPhotoViewer.Bussines.LocalDataManager
{
    public class LocalDataManager : ILocalDataManager
    {
        private const string FileName = "NinjaSaveDataFile.txt";

        public async Task SaveLocalData(LocalDataDTO localDataDTO)
        {
          string data = JsonConvert.SerializeObject(localDataDTO);
          await SaveDataToFileAsync(data);
        }

        public async Task<LocalDataDTO> LoadLocalData()
        {
            string localData = await ReadDataFromFileAsync();
            if (string.IsNullOrEmpty(localData))
                return null;

            return JsonConvert.DeserializeObject<LocalDataDTO>(localData);
        }


        private async Task SaveDataToFileAsync(string data)
        {
            StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            StorageFile saveFile = await storageFolder.CreateFileAsync(FileName, CreationCollisionOption.ReplaceExisting);
            await FileIO.WriteTextAsync(saveFile, data);
        }

        private async Task<string> ReadDataFromFileAsync()
        {
            StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            IStorageItem file =  await storageFolder.TryGetItemAsync(FileName);

            if (file == null)
                return null;

            StorageFile dataFile = await storageFolder.GetFileAsync(FileName);

            return await FileIO.ReadTextAsync(dataFile);
        }
    }
}