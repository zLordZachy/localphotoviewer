﻿
using System.Collections.Generic;

namespace LocalPhotoViewer.DTO
{
    public class Photo
    {
        public string id { get; set; }
        public string owner { get; set; }
        public string secret { get; set; }
        public string server { get; set; }
        public int farm { get; set; }
        public string title { get; set; }
        public int ispublic { get; set; }
    //    public Location location { get; set; }
        public double? latitude { get; set; }
        public double? longitude { get; set; }
        public string accuracy { get; set; }
        public string context { get; set; }
        public string place_id { get; set; }
        public string woeid { get; set; }
        public string geo_is_family { get; set; }
        public string geo_is_friend { get; set; }
        public string geo_is_contact { get; set; }
        public string geo_is_public { get; set; }
    }

    public class Photos
    {
        public int page { get; set; }
        public int pages { get; set; }
        public int perpage { get; set; }
        public int total { get; set; }
        public List<Photo> photo { get; set; }
    }

    public class RootObject
    {
        public Photos photos { get; set; }
        public string stat { get; set; }
    }
    
}
