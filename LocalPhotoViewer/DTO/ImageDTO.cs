﻿using System;
using Windows.UI.Xaml.Media.Imaging;

namespace LocalPhotoViewer.DTO
{
    public class ImageDTO
    {
        public BitmapImage Image { get; set; }
        public Uri ImgUrl { get; set; }
        public Photo PhotoData {get; set; }
        public string PhotoId { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string Title { get; set; }
    }
}
