﻿
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace LocalPhotoViewer.DTO
{
    public class LocalDataDTO
    {
        public int Radius { get; set; }
        public string Page { get; set; }
        public bool SortAsc { get; set; }
        public string MaxPage { get; set; }
        public ObservableCollection<ImageDTO> Images { get; set; }
        public LocationDTO Location { get; set; }
    }
}
