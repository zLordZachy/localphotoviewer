﻿using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using LocalPhotoViewer.Bussines.ApiClient;
using LocalPhotoViewer.Bussines.LocalDataManager;
using LocalPhotoViewer.Bussines.LocationProvider;
using LocalPhotoViewer.BussinesLogick.LocationProvider;
using LocalPhotoViewer.ViewModel;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238
namespace LocalPhotoViewer.View
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class LocationPage
    {
        private readonly PhotoViewModel _viewModel;

        public LocationPage()
        {
            InitializeComponent();
            _viewModel = new PhotoViewModel(new LocationProvider(), new FliperApiClient(), new LocalDataManager());
            DataContext = _viewModel;
        }

        private void LocationPage_OnLoaded(object sender, RoutedEventArgs e)
        {
            _viewModel.OnLoaded();
        }

        private void TextBox_OnBeforeTextChanging(TextBox sender,
            TextBoxBeforeTextChangingEventArgs args)
        {
            args.Cancel = args.NewText.Any(c => !char.IsDigit(c));
        }
    }
}
