﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using LocalPhotoViewer.Bussines.LocalDataManager;
using LocalPhotoViewer.Bussines.LocationProvider;
using LocalPhotoViewer.ViewModel;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace LocalPhotoViewer.View
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MapPage : Page
    {
        private readonly MapViewModel _mapViewModel;

        public MapPage()
        {
            this.InitializeComponent();
            MapControl a = BaseControl;
            _mapViewModel = new MapViewModel(BaseControl, new LocationProvider(), new LocalDataManager());
            DataContext = _mapViewModel;
        }

        private void MapPage_OnLoaded(object sender, RoutedEventArgs e)
        {
            _mapViewModel.OnLoaded();
        }
    }
}
