﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using LocalPhotoViewer.View;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace LocalPhotoViewer
{
    /// <summary>
    ///     An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage
    {
        public Frame NavigationFrame => ContentFrame;

        public MainPage()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            ContentFrame.Navigate(typeof(LocationPage));
        }

        private void nvTopLevelNav_ItemInvoked(NavigationView sender, NavigationViewItemInvokedEventArgs args)
        {

            if (args.IsSettingsInvoked)
            {
                ContentFrame.Navigate(typeof(SettingsPage));
            }
            else
            {
                if (args.InvokedItem is string itemContent)
                {
                    switch (itemContent)
                    {
                        case "Photo":
                            ContentFrame.Navigate(typeof(LocationPage));
                            break;

                        case "Map":
                            ContentFrame.Navigate(typeof(MapPage));
                            break;
                    }
                }
            }
        }
    }
}