﻿using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using LocalPhotoViewer.Bussines.ApiClient;
using LocalPhotoViewer.Bussines.LocalDataManager;
using LocalPhotoViewer.BussinesLogick.LocationProvider;
using LocalPhotoViewer.DTO;
using LocalPhotoViewer.helprs;

namespace LocalPhotoViewer.ViewModel
{
    public class PhotoViewModel : BaseViewModel
    {
        private readonly ILocationProvider _locationProvider;
        private readonly IFliperApiClient _fliperApiClient;
        private readonly ILocalDataManager _localDataManager;

        public LocationDTO Location
        {
            get => _location;
            set
            {
                _location = value;
                OnPropertyChanged();
            }
        }

        public Visibility VisibilityImage
        {
            get => _visibilityImage;
            set
            {
                _visibilityImage = value;
                OnPropertyChanged();
            }
        }

        public Visibility VisibilityButton
        {
            get => _visibilityButton;
            set
            {
                _visibilityButton = value;
                OnPropertyChanged();
            }
        }

        public int Radius
        {
            get => _radius;
            set
            {
                _radius = value;
                OnPropertyChanged();
            }
        }

        public string Page
        {
            get => _page.ToString();
            set
            {
                 Int32.TryParse(value, out int valuInt);
                _page = valuInt;
                OnPropertyChanged();
            }
        }

        public bool SortAsc
        {
            get => _sortAsc;
            set
            {
                _sortAsc = value;
                OnPropertyChanged();
            }
        }

        public string MaxPage
        {
            get => _maxPage;
            set
            {
                _maxPage = value;
                OnPropertyChanged();
            }
        }

        public bool IsBusy
        {
            get => _isBusy;
            set
            {
                _isBusy = value;
                OnPropertyChanged();
            }
        }

        public ZCommand RefreshCommand { get; set; }

        public ZCommand UnselectImage { get; set; }

        public ObservableCollection<ImageDTO> Imgs
        {
            get => _imgs;
            set
            {
                _imgs = value;
                OnPropertyChanged();
            }
        }

        public ImageDTO SelectedImage
        {
            get => _selectedImage;
            set
            {
                _selectedImage = value;
                OnPropertyChanged();
                OpenImage(value);
            }
        }


        public PhotoViewModel(ILocationProvider locationProvider, IFliperApiClient fliperApiClient, ILocalDataManager localDataManager)
        {
            _locationProvider = locationProvider;
            _fliperApiClient = fliperApiClient;
            _localDataManager = localDataManager;
            RefreshCommand = new ZCommand(o => true, Refresh);
            UnselectImage = new ZCommand(o => true, UnselectImageDTO);
            Imgs = new ObservableCollection<ImageDTO>();
            VisibilityImage = Visibility.Collapsed;
            VisibilityButton = Visibility.Collapsed;
            Radius = 5;
            Page = "1";
            SortAsc = false;
            IsBusy = true;
            MaxPage = "";
        }

        private void UnselectImageDTO(object obj)
        {
            VisibilityImage = Visibility.Collapsed;
            VisibilityButton = Visibility.Collapsed;
            SelectedImage = null;
        }

        private void OpenImage(ImageDTO obj)
        {
            if(obj == null)
                return;

            VisibilityImage = Visibility.Visible;
            VisibilityButton = Visibility.Visible;
        }

        private async void Refresh(object obj)
        {
            try
            {

           
            IsBusy = true;
            
            Location = await _locationProvider.GetCurrentLocationAsync();
            if (Location.Longitude == null || Location.Latitude == null)
            {
                IsBusy = false;
                return;
            }

            RootObject data = await _fliperApiClient.GetImagesAsync(Location.Longitude.Value, Location.Latitude.Value, Radius, _page, SortAsc);

            if (data.photos == null)
            {
                IsBusy = false;
                return;
            }

            MaxPage = $"{data.photos.page} of {data.photos.pages}";
            ObservableCollection<ImageDTO> images = LoadData(data);
            IsBusy = false;

            LocalDataDTO localData = new LocalDataDTO
            {
                Images = images,
                Page = Page,
                Radius = Radius,
                SortAsc = SortAsc,
                Location = Location,
                MaxPage = MaxPage
            };

            await _localDataManager.SaveLocalData(localData);
            }
            catch (Exception)
            {
                ContentDialog noWifiDialog = new ContentDialog
                {
                    Title = "No wifi or GPS connection",
                    Content = "Check your connection and try again.",
                    CloseButtonText = "Ok"
                };

                await noWifiDialog.ShowAsync();
                IsBusy = false;
            }
        }

     
        public async void OnLoaded()
        {
            IsBusy = true;
            var localData = await _localDataManager.LoadLocalData();

            if (localData == null)
            {
                IsBusy = false;
                return;
            }

            Radius = localData.Radius;
            Page = localData.Page;
            SortAsc = localData.SortAsc;
            Location = new LocationDTO();
            Imgs = localData.Images;
            MaxPage = localData.MaxPage;
            IsBusy = false;
        }

        private ObservableCollection<ImageDTO> LoadData(RootObject data)
        {
            if (data.stat != "ok")
                return new ObservableCollection<ImageDTO>();

            Imgs.Clear();

            foreach (Photo photo in data.photos.photo)
            {
                Imgs.Add(CreateImage(photo));
            }

            return Imgs;
        }

        private ImageDTO CreateImage(Photo photo)
        {
            var imgUrl = new Uri($"http://farm{photo.farm}.static.flickr.com/{photo.server}/{photo.id}_{photo.secret}.jpg");
            BitmapImage bitmapImage = new BitmapImage();
            bitmapImage.UriSource = imgUrl;

            ImageDTO image = new ImageDTO
            {
                Image = bitmapImage,
                ImgUrl = imgUrl,
                PhotoId = photo.id,
                Longitude = photo.longitude,
                Latitude = photo.latitude,
                Title = photo.title
            };
            return image;
        }

        private LocationDTO _location;
        private int _radius;
        private int _page;
        private bool _sortAsc;
        private bool _isBusy;
        private ObservableCollection<ImageDTO> _imgs;
        private string _maxPage;
        private ImageDTO _selectedImage;
        private Visibility _visibilityImage;
        private Visibility _visibilityButton;
    }
}
