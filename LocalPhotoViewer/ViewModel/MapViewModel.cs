﻿
using System;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Graphics.Imaging;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using LocalPhotoViewer.Bussines.LocalDataManager;
using LocalPhotoViewer.BussinesLogick.LocationProvider;
using LocalPhotoViewer.DTO;
using System.Collections.ObjectModel;

namespace LocalPhotoViewer.ViewModel
{
    public class MapViewModel : BaseViewModel
    {
        private readonly ILocationProvider _locationProvider;
        private readonly ILocalDataManager _localDataManager;
        ObservableCollection<MapElement> myLandmarks = new ObservableCollection<MapElement>();


        public MapControl MapData
        {
            get => _mapData;
            set
            {
                _mapData = value;
                OnPropertyChanged();
            }
        }

        public bool IsBusy
        {
            get => _isBusy;
            set
            {
                _isBusy = value;
                OnPropertyChanged();
            }
        }


        public MapViewModel(MapControl mapControl, ILocationProvider locationProvider, ILocalDataManager localDataManager)
        {
            MapData = mapControl;
            _locationProvider = locationProvider;
            _localDataManager = localDataManager;
        }


        public async void OnLoaded()
        {
            try
            {


                IsBusy = true;
                LocationDTO location = await _locationProvider.GetCurrentLocationAsync();
                if (!location.Latitude.HasValue || !location.Longitude.HasValue)
                    return;

                BasicGeoposition actualPositon = new BasicGeoposition()
                    {Latitude = location.Latitude.Value, Longitude = location.Longitude.Value};
                Geopoint geopoint = new Geopoint(actualPositon);
                MapData.Center = geopoint;
                MapData.ZoomLevel = 12;
                MapData.LandmarksVisible = true;

                BasicGeoposition snPosition = new BasicGeoposition
                    {Latitude = location.Latitude.Value, Longitude = location.Longitude.Value};
                Geopoint snPoint = new Geopoint(snPosition);

                LocalDataDTO localdata = await _localDataManager.LoadLocalData();

                if (localdata == null)
                {
                    IsBusy = false;
                    return;
                }

                var landmarksLayer = new MapElementsLayer
                {
                    ZIndex = 1,
                    MapElements = myLandmarks
                };

                MapData.Layers.Add(landmarksLayer);


                foreach (ImageDTO imageDTO in localdata.Images)
                {

                    if (imageDTO.Longitude != null && imageDTO.Latitude != null)
                    {
                        BasicGeoposition snPosition2 = new BasicGeoposition
                            {Latitude = imageDTO.Latitude.Value, Longitude = imageDTO.Longitude.Value};
                        Geopoint snPoint2 = new Geopoint(snPosition2);

                        MapIcon mapIcon = new MapIcon
                        {
                            Location = snPoint2,
                            NormalizedAnchorPoint = new Point(0.5, 1.0),
                            ZIndex = 0,
                            Image = await CreateNewImage(imageDTO.ImgUrl)
                        };

                        myLandmarks.Add(mapIcon);
                    }
                }

                MapData.Center = snPoint;
                IsBusy = false;

            }
            catch (Exception)
            {
                ContentDialog noWifiDialog = new ContentDialog
                {
                    Title = "No wifi or GPS connection",
                    Content = "Check your connection and try again.",
                    CloseButtonText = "Ok"
                };

                await noWifiDialog.ShowAsync();
                IsBusy = false;
            }
        }

        private async Task<IRandomAccessStreamReference> CreateNewImage(Uri uri)
        {
            RandomAccessStreamReference randomAccStr = RandomAccessStreamReference.CreateFromUri(uri);
            return await ResizeImage(randomAccStr, 64, 64);
        }

        private async Task<RandomAccessStreamReference> ResizeImage(RandomAccessStreamReference imageFile, uint scaledWidth, uint scaledHeight)
        {
            using (IRandomAccessStream fileStream = await imageFile.OpenReadAsync())
            {
                BitmapDecoder decoder = await BitmapDecoder.CreateAsync(fileStream);

                //create a RandomAccessStream as output stream
                var memStream = new InMemoryRandomAccessStream();

                BitmapEncoder encoder = await BitmapEncoder.CreateForTranscodingAsync(memStream, decoder);

                encoder.BitmapTransform.ScaledWidth = scaledWidth;
                encoder.BitmapTransform.ScaledHeight = scaledHeight;

                //commits and flushes all of the image data
                await encoder.FlushAsync();

                return RandomAccessStreamReference.CreateFromStream(memStream);
            }
        }

        private MapControl _mapData;
        private bool _isBusy;
    }
}
